configfile: "config.yaml"
print(config["mg_samples"])



def aggregate_msas(wildcards):
	cp_output=checkpoints.make_multi_seq.get(sample=wildcards.sample).output[0]
	return expand("msa/{sample}/{j}.aln", sample=wildcards.sample, j=glob_wildcards(os.path.join(cp_output, "{j}")).j)

def aggregate_rnacode(wildcards):
	cp_output=checkpoints.make_multi_seq.get(sample=wildcards.sample).output[0]
	return expand("rnacoderesults/small_proteins_{sample}/{j}.fa", sample=wildcards.sample, j=glob_wildcards(os.path.join(cp_output, "{j}")).j)

def aggregate_hmm(wildcards):
	cp_output=checkpoints.sortByFamID.get(sample=wildcards.sample).output[0]
	return expand("small_proteins/hmms/{sample}/{i}.hmm", sample=wildcards.sample, i=glob_wildcards(os.path.join(cp_output, "{i}.fa")).i)



rule all:
	input:
		expand("small_proteins/GFF/{sample}.gff", sample=config["mg_samples"]),
		expand("small_proteins/representativeSeqs/repSeqs_{sample}.faa", sample=config["mg_samples"]),
		expand("small_proteins/small_proteins_{sample}.faa", sample=config["mg_samples"]),
		expand("small_proteins/hmms/{sample}_aggregated.hmm", sample=config["mg_samples"])


rule preprocess_contigs:
	input:
		lambda wildcards: config["mg_samples"][wildcards.sample]
	output:
		"preprocessed_contigs/{sample}.fa"
	conda:
		"envs/smallproteins.yml"
	script:
		"scripts/preprocess_contigs.py"

rule prodigalsmall:
	input:
		"preprocessed_contigs/{sample}.fa"
	output:
		prot="ORFs_prot/ORFs_{sample}",
		nuc="ORFs_nuc/ORFs_{sample}",
		gff="prodigalsmall_gff/output_{sample}.gff"
	conda: 
		"envs/smallproteins.yml"
	shell:
		"prodigalsmall -a {output.prot} -d {output.nuc}  -i {input} -o {output.gff} -f gff -p meta"

rule filtersORFs:
	input:
		"ORFs_prot/ORFs_{sample}"
	output:
		"potential_sORFs/sORFs_{sample}.faa"
	conda: 
		"envs/smallproteins.yml"
	script:
		"scripts/filtersORFs.py"

rule cd_hit:
	input:
		"potential_sORFs/sORFs_{sample}.faa"
	output:
		rep="clusters/spclusters_{sample}",
		clstr="clusters/spclusters_{sample}.clstr"
	conda: 
		"envs/smallproteins.yml"
	shell:
		"cd-hit -i {input} -o {output.rep} -n 2 -p 1 -c 0.5 -d 0 -M 50000 -l 5 -s 0.95 -aL 0.95 -g 1"

checkpoint make_multi_seq:
	input:
		nucs="ORFs_nuc/ORFs_{sample}",
		clstr="clusters/spclusters_{sample}.clstr"
	output:
		directory("multi-seq/{sample}_dir")
	conda: 
		"envs/smallproteins.yml"
	shell:
		"make_multi_seq.pl {input.nucs} {input.clstr} {output} 8"

rule filterMultiSeqforRNAcode:
	input:
		"multi-seq/{sample}_dir/{j}"
	output:
		"multi-seq/{sample}_dir_filtered/{j}"
	conda:
		"envs/smallproteins.yml"
	script:
		"scripts/filterMultiSeqforRNAcode.py"

rule mafftclusters:
	input:
		"multi-seq/{sample}_dir_filtered/{j}"
	output:
		"msa/{sample}/{j}.aln"
	conda: 
		"envs/smallproteins.yml"
	shell:
		"ginsi --clustalout --maxiterate 2 {input} > {output}"

rule aggregatemsa:
	input:
		aggregate_msas
	output:
		"msa/msa_{sample}.aln"
	shell:
		"cat {input} > {output}"

rule rnacode:
	input:
		"msa/{sample}/{j}.aln"
	output:
		output="rnacoderesults/rnacoderesults_{sample}/{j}.gtf"
	conda: 
		"envs/smallproteins.yml"
	shell:
		"RNAcode --eps --eps-cutoff 0.05 --eps-dir rnacoderesults/rnacodeplots_{wildcards.sample}/{wildcards.j} --outfile {output.output} --gtf --best-only --cutoff 0.05 {input}"

rule filterClstrsByRNAcodeOutput:
	input:
		"rnacoderesults/rnacoderesults_{sample}/{j}.gtf",
		"multi-seq/{sample}_dir/{j}"
	output:
		"rnacoderesults/small_proteins_{sample}/{j}.fa"
	conda: 
		"envs/smallproteins.yml"
	script:
		"scripts/filterClstrsByRNAcodeOutput.py"

rule aggregateRNAcoderesults:
	input:
		aggregate_rnacode
	output:
		"small_proteins/sORFs_{sample}.fna"
	conda: 
		"envs/smallproteins.yml"
	script:
		"scripts/aggregateRNAcoderesults.py"		

rule collectRepSeqs:
	input:
		"small_proteins/sORFs_{sample}.fna"
	output:
		"small_proteins/representativeSeqs/repSeqs_{sample}.fna"
	conda:
		"envs/smallproteins.yml"
	script:
		"scripts/collectRepSeqs.py"

rule getProtRepSeqs:
	input:
		"small_proteins/representativeSeqs/repSeqs_{sample}.fna",
		"potential_sORFs/sORFs_{sample}.faa"
	output:
		"small_proteins/representativeSeqs/repSeqs_{sample}.faa"
	conda:
		"envs/smallproteins.yml"
	script:
		"scripts/getProtRepSeqs.py"

rule getProtSeqs:
	input:
		"small_proteins/sORFs_{sample}.fna",
		"potential_sORFs/sORFs_{sample}.faa"
	output:
		"small_proteins/small_proteins_{sample}.faa"
	conda:
		"envs/smallproteins.yml"
	script:
		"scripts/getProtSeqs.py"

rule cleanGFFfiles:
	input:
		"prodigalsmall_gff/output_{sample}.gff"
	output:
		"prodigalsmall_gff/clean_{sample}.gff"
	conda:
		"envs/smallproteins.yml"
	script:
		"scripts/cleanGFFfiles.py"

rule correctGFFIDs:
	input:
		"prodigalsmall_gff/clean_{sample}.gff"
	output:
		"prodigalsmall_gff/tidy_{sample}.gff"
	conda:
		"envs/smallproteins.yml"
	script:
		"scripts/correctGFFIDs.py"

rule filterGFFfiles:
	input:
		"small_proteins/sORFs_{sample}.fna",
		"prodigalsmall_gff/tidy_{sample}.gff"
	output:
		"prodigalsmall_gff/gffutils_db/{sample}.db",
		"small_proteins/GFF/{sample}.gff"
	conda: 
		"envs/smallproteins.yml"
	script:
		"scripts/filterGFFfiles.py"





checkpoint sortByFamID:
	input:
		"small_proteins/small_proteins_{sample}.faa"
	output:
		directory("small_proteins/families/{sample}_fams")
	conda: 
		"envs/smallproteins.yml"
	script:
		"scripts/sortByFamID.py"

rule mafftfamilies:
	input:
		"small_proteins/families/{sample}_fams/{i}.fa"
	output:
		temp("small_proteins/tmp/{sample}/{i}.aln")
	conda: 
		"envs/smallproteins.yml"
	shell:
		"ginsi --clustalout --maxiterate 4 {input} > {output}"

rule replaceClustalHeader:
	input:
		"small_proteins/tmp/{sample}/{i}.aln"
	output:
		"small_proteins/msa/{sample}/{i}.aln"
	conda: 
		"envs/smallproteins.yml"
	script:
		"scripts/replaceClustalHeader.py"

rule hmmbuild:
	input:
		"small_proteins/msa/{sample}/{i}.aln"
	output:
		"small_proteins/hmms/{sample}/{i}.hmm"
	conda: 
		"envs/smallproteins.yml"
	shell:
		"hmmbuild -n SmallProtein_{wildcards.i} {output} {input}"

rule aggregate:
	input:
		aggregate_hmm
	output:
		"small_proteins/hmms/{sample}_aggregated.hmm"
	shell:
		"cat {input} > {output}"



