# Small Protein Prediction Pipeline

This Pipeline implements the methods described by Sberro, *et al.* in their paper "Large-Scale Analyses of Human Microbiomes Reveal Thousands of Small, Novel Genes" (2019, https://doi.org/10.1016/j.cell.2019.07.016) as a reproducible workflow to predict small coding sequences in metagenomes (sORFs <= 150 base pairs in length) and builds HMM profiles of the small proteins they encode.

## Prerequisites

The Small Protein Prediction Pipeline is implemented with the Snakemake module for workflow control and uses the environment and package manager conda for automated deployment of software dependencies, so the only prerequisites needed are: 

```bash
- conda 4.8.2+
- snakemake 5.7.1+
```

