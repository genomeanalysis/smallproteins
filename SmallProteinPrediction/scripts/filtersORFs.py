'!usr/bin/python'
import sys
import re
import Bio
from Bio import SeqIO

with open(snakemake.output[0], 'w') as outfile:
	for record in SeqIO.parse(snakemake.input[0], "fasta"):
		if (re.search('partial=00', record.description)):
			if (len(record.seq) < 51):				
				outfile.write(">" + record.description + "\n")
				outfile.write(str(record.seq) + "\n")
