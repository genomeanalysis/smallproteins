'!usr/bin/python'
import re
import Bio
from Bio import SeqIO

with open(snakemake.input[0], 'r') as gtf, open(snakemake.input[1], 'r') as seqs, open(snakemake.output[0], 'w') as out:
	line = gtf.readline()
	if line:
		for rec in SeqIO.parse(seqs, 'fasta'):
			out.write(">" + re.search("M[0-9]_[0-9]_V[0-9]_[0-9]{1,8}_[0-9]{1,7}", rec.description).group() + "\n")
			out.write(str(rec.seq) + "\n")