'!usr/bin/python'
import re
import Bio
from Bio import SeqIO

with open(snakemake.output[0], 'w') as out:
	i = 1
	for rec in SeqIO.parse(snakemake.input[0], 'fasta'):
		if re.search("famID:" + str(i), rec.description):
			out.write(">" + rec.description + "\n")
			out.write(str(rec.seq) + "\n")
			i = i+1