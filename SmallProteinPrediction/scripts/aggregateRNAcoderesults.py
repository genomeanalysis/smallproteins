'!usr/bin/python'
import Bio
from Bio import SeqIO

with open(snakemake.output[0], 'w') as out:
	famID = 1
	for fam in snakemake.input:
		cds = False
		for rec in SeqIO.parse(fam, 'fasta'):
			out.write(">" + rec.description + "_famID:" + str(famID) + "\n")
			out.write(str(rec.seq) + "\n")
			cds = True
		if cds == True:
			famID = famID+1