'!usr/bin/python'
import gffutils
import re
import Bio
from Bio import SeqIO

gffutils.create_db(snakemake.input[1], snakemake.output[0], sort_attribute_values=True)

db = gffutils.FeatureDB(snakemake.output[0])



with open(snakemake.output[1], 'w') as out:
	for rec in SeqIO.parse(snakemake.input[0], 'fasta'):
		gene = db[re.search("M[0-9]_[0-9]_V[0-9]_[0-9]{1,8}_[0-9]{1,7}", rec.description).group()]
		out.write(re.sub("(?<=^M[0-9]_[0-9]_V[0-9])(?=_[0-9]{1,8})", "_contig", str(gene)) + "\n") 
