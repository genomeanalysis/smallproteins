'!usr/bin/python'
import re

with open(snakemake.input[0], 'r') as gff, open(snakemake.output[0], 'w') as out:
        
        for line in gff.readlines():
                contig = re.search("M[0-9]_[0-9]_V[0-9]_[0-9]{1,8}", line).group()
                out.write(re.sub("(?<=ID=)[0-9]{1,8}", contig, line))


            