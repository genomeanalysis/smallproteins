'!usr/bin/python'
import re
import os
import Bio
from Bio import SeqIO

def writeOutfile(id, data):
	with open("small_proteins/families/" + snakemake.wildcards.sample + "_fams/famID_" + str(id) + ".fa", 'w') as out:
		for entry in data:
			out.write(entry + "\n")

os.mkdir("small_proteins/families/" + snakemake.wildcards.sample + "_fams")
famID = 1
data = []
for rec in SeqIO.parse(snakemake.input[0], 'fasta'):
	if re.search("famID:" + str(famID), rec.description):
		data.append(">" + rec.description)
		data.append(str(rec.seq))
	else:
		writeOutfile(famID, data)
		data = []
		famID = famID+1
		data.append(">" + rec.description)
		data.append(str(rec.seq))
writeOutfile(famID, data)



