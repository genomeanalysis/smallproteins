'!usr/bin/python'
import re
import Bio
from Bio import SeqIO

with open(snakemake.output[0], 'w') as out:
	i = 1
	for rec in SeqIO.parse(snakemake.input[0], 'fasta'):
		if i <= 100:
			out.write(">" + re.sub("(?<=M[0-9][0-9])_", "", re.sub("(?<=M[0-9])_", "", rec.description)) + "\n")
			out.write(str(rec.seq) + "\n")
			i = i + 1
