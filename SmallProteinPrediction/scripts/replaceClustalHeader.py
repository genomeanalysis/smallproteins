'!usr/bin/python'
import shutil

with open(snakemake.input[0], 'r') as from_file, open(snakemake.output[0], 'w') as to_file:
	line = from_file.readline()
	line = "CLUSTAL X (1.81) multiple sequence alignment"
	to_file.write(line)
	shutil.copyfileobj(from_file, to_file)