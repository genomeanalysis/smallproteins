'!usr/bin/python'
import re

with open(snakemake.input[0], 'r') as prodigalsmallGFF, open(snakemake.output[0], 'w') as out:
        line = prodigalsmallGFF.readline()
        while line:
                if re.search("^M", line):
                        out.write(line)
                        line = prodigalsmallGFF.readline()
                else: 
                        line = prodigalsmallGFF.readline()

