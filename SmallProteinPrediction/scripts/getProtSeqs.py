'!usr/bin/python'
import re
import Bio
from Bio import SeqIO


with open(snakemake.output[0], 'w') as out:
    ProtSeqs = list(SeqIO.parse(snakemake.input[1], 'fasta'))
    for rec in SeqIO.parse(snakemake.input[0], 'fasta'):
        out.write(">" + rec.description + "\n")
        seqid = re.search(".*(?=_famID)", rec.description).group()
        i = 0
        while not re.search(seqid, ProtSeqs[i].description):
            i = i+1
        out.write(str(ProtSeqs[i].seq) + "\n")
