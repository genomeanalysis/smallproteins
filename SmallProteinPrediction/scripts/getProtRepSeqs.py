'!usr/bin/python'
import re
import Bio
from Bio import SeqIO


with open(snakemake.output[0], 'w') as out:
	for rec in SeqIO.parse(snakemake.input[0], 'fasta'):
		out.write(">" + rec.description + "\n")
		for seq in SeqIO.parse(snakemake.input[1], 'fasta'):
			if re.search(re.search("M[0-9]_[0-9]_V[0-9]_[0-9]{1,8}_[0-9]{1,7}", rec.description).group(), seq.description):
				out.write(str(seq.seq) + "\n")
				break
	