'!usr/bin/python'
import re
import csv
import subprocess


sam = subprocess.run(["samtools", "view", "-c", "-F", "260", str(snakemake.input[0])], stdout=subprocess.PIPE)

sam.check_returncode()

totalReads = float(sam.stdout)

i = 1

with open(snakemake.input[1], 'r') as counts, open(snakemake.output[0], 'w') as out:
	rdr = csv.reader(counts, delimiter='	')
	for entry in rdr:
		if i > 2:
			out.write(entry[0] + "	")
			out.write(str(1000000000 * (float(entry[6]) / (totalReads * float(entry[5])))) + "\n")
		else:
			i = i + 1